from requests.auth import HTTPBasicAuth
from zeep.wsse.signature import Signature
from zeep.transports import Transport
from requests import Session, Request
from zeep import Client, Settings
import urllib3
import time

session = Session()
session.verify = False
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

session.auth = HTTPBasicAuth('sms01','Conax123')
transport = Transport(session=session)


def checkCard(cardid):
    # proveriava za kakvi paketi e abonirana kartata
    # struct={"nomer":{"add":{},
    #                "delete":{}
    #                }
    #         }
    struct={}
    g = Client('https://192.168.100.250:44302/ca-server/webservices/caclient/subscription?wsdl',transport=transport)
    request_data = {"GetActiveSubscriptionsRequest": {"CaClientId": str(cardid)}}
    rezult = g.service.GetActiveSubscriptions(**request_data)
    if rezult['TransactionStatus']['StatusCode']=='OK':
        if rezult['SubscriptionStatuses'] is not None:
            for package in rezult['SubscriptionStatuses']['SubscriptionStatus']:
                # print 'Subscribed for package {}'.format(package['Subscription']['ProductId'])
                struct.setdefault(str(cardid),{}).setdefault("add",[]).append(package["Subscription"]["ProductId"])
        else:
            # print 'card {} doesn\'t have any package assigned'.format(cardid)
            struct.setdefault(str(cardid),{})["add"]=[]
    else:
        print 'card {} doesn\'t exist'.format(cardid)
    return struct

def modifyCard(cardid,action,package=''):
    #dobavia ili premahva paketite za koieto e abonirana kartata
    pass

def addSub(cardid,package):
    g = Client('https://192.168.100.250:44302/ca-server/webservices/caclient/subscription?wsdl', transport=transport)
    request_data = {"AddSubscriptionsRequest": {"Subscriptions": {
        "Subscription": {
        "CaClientId":cardid,
        "ProductId":package
        }}

    }}
    g.service.AddSubscriptions(**request_data)

def cancelSub(cardid,package):
    g = Client('https://192.168.100.250:44302/ca-server/webservices/caclient/subscription?wsdl', transport=transport)
    request_data = {"CancelSubscriptionsRequest": {"Subscriptions": {
        "Subscription": {
        "CaClientId":cardid,
        "ProductId":package
        }}

    }}
    g.service.CancelSubscriptions(**request_data)

def populateDb():
    pass


#checkCard([str(i) for i in range(2082102001,2082102011)])
# checkCard('2082104003')