#!/usr/bin/env python
from flask import Flask,request
from flask import render_template
from flask import jsonify as jsn
from soap import checkCard,addSub,cancelSub

app=Flask(__name__)

@app.route('/',methods=['GET','POST'])
def index():
    return render_template('test01.html')

@app.route('/soapi',methods=['GET', 'POST'])
def soapi():
    tempValue = request.get_json()
    for(k,v) in tempValue.items():
        for(m,n) in v.items():
            if(m=="add"):
                for(num,packname) in n.items():
                    print("adding package {}".format(num))
                    addSub(k,num)
            else:
                for (num, packname) in n.items():
                    print("deleting package {}".format(num))
                    cancelSub(k,num)
            # print('-- {} : {} - {} \n'.format(k,m,n))
    return 'OK',200
    
@app.route('/checkCardNumber/<int:id>',methods=['GET'])
def checkCardNumber(id):
    # tempValue = request.get_json()
    dat=checkCard(id)
    print "----- > {} - {} ".format(id,dat)
    return jsn(dat)

if __name__ == '__main__':
    app.run(host='192.168.98.197',port=8010)

