#!/usr/bin/env python
from flask import Flask
from flask import render_template

app=Flask(__name__)

@app.route('/',methods=['GET','POST'])
def index():
    return render_template('test01.html')

@app.route('/soapi',methods=['GET', 'POST'])
def soapi():
    return 'data'
    

if __name__ == '__main__':
    app.run(host='192.168.8.110',port=80)

